<?php if (empty($authorisations)) { ?>
	<p>There are no authorisations for this venue.</p>
<?php } else { ?>
	<div style="overflow-x:auto;">
	<table class="non-grocery-table">
		<tr>
			<th>
				Title
			</th>
			<th>
				Name
			</th>
			<th>
				ID Card
			</th>
			<th>
				Event
			</th>
			<th>
				Date
			</th>
		</tr>
		<?php foreach($authorisations as $auth) { ?>
			<tr>
				<td>
					<?php echo $auth['title'] . '<br />'; ?>
				</td>
				<td>
					<?php echo $auth['name'] . '<br />'; ?>
				</td>
				<td>
					<?php echo $auth['id'] . '<br />'; ?>
				</td>
				<td>
					<?php echo $auth['event'] . '<br />'; ?>
				</td>
				<td>
					<?php echo $auth['date'] . '<br />'; ?>
				</td>
			</tr>
		<?php } ?>
	</table>
	</div>
<?php } ?>
