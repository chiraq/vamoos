<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>VAMOOS System<?php if (isset($title)) { echo ' | ' . $title; } ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('css/normalize.css'); ?>" />
	<?php if(isset($output)): ?>
		<?php foreach($css_files as $file): ?>
			<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
		<?php endforeach; ?>
	<?php endif ?>
	<?php if(isset($output)): ?>
		<?php foreach($js_files as $file): ?>
			<script src="<?php echo $file; ?>"></script>
		<?php endforeach; ?>
	<?php endif ?>
	<link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>" />
</head>
<body>
<header>
	<nav>
		<ul>
			<li>
				<a class="<?php if($this->uri->segment(1)!="main"){echo "active";}?>" 
					href='<?php echo site_url('')?>'>Home</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="sports"){echo "active";}?>" 
					href='<?php echo site_url('main/sports')?>'>Sports</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="venues"){echo "active";}?>" 
					href='<?php echo site_url('main/venues')?>'>Venues</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="fixtures"){echo "active";}?>" 
					href='<?php echo site_url('main/fixtures')?>'>Fixtures</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="officials"){echo "active";}?>" 
					href='<?php echo site_url('main/officials')?>'>Officials</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="cards"){echo "active";}?>" 
					href='<?php echo site_url('main/cards')?>'>Cards</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="authorisations"){echo "active";}?>" 
					href='<?php echo site_url('main/authorisations')?>'>Authorisations</a>
			</li>
			<li>
				<a class="<?php if($this->uri->segment(2)=="accesses"){echo "active";}?>" 
					href='<?php echo site_url('main/accesses')?>'>Accesses</a>
			</li>
		</ul>
	</nav>
	<h1><?php echo $title; ?></h1>
</header>
<div id="main">
