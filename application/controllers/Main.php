<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();	 
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->library('table');
	}

	/* Private functions */

	private function find_valid_from_date($sport_id) 
	{
		$this->db->select('events_start');
		$this->db->from('official');
		$this->db->join('sport', 'sport.id = official.sport_id', 'inner');
		$this->db->where('sport.id', $sport_id);
		$query = $this->db->get();

		if ($query->num_rows() > 0) 
		{ 
			$row = $query->row(); 
			return $row->events_start;
		}
	}

	private function find_valid_until_date($sport_id) 
	{
		$this->db->select('events_finish');
		$this->db->from('official');
		$this->db->join('sport', 'sport.id = official.sport_id', 'inner');
		$this->db->where('sport.id', $sport_id);
		$query = $this->db->get();

		if ($query->num_rows() > 0) 
		{ 
			$row = $query->row(); 
			return $row->events_finish;
		}
	}

	private function deactivate_old_cards() 
	{
		$date_now = date("Y-m-d");

		$data = array('active' => "No");

		$this->db->select('card.id');
		$this->db->from('card');
		$this->db->join('official', 'official.id = card.official_id', 'inner');
		$this->db->join('sport', 'official.sport_id = sport.id', 'inner');
		$this->db->where('sport.events_finish <', $date_now);
		$query = $this->db->get();

		foreach ($query->result() as $row) 
		{
			$id = $row->id;
			$this->db->where('id', $id);
			$this->db->update('card', $data); 
		}
	}

	private function cancel_old_authorisations() 
	{ 
		$this->db->select('card.id');
		$this->db->from('card');
		$this->db->join('authorisation', 'authorisation.card_id = card.id', 'inner');
		$this->db->where('card.active =', "No");
		$query = $this->db->get();

		foreach ($query->result_array() as $row) 
		{
			$id = $row['id'];
			echo $id;
			$this->db->where('card_id', $id);
			$this->db->delete('authorisation'); 
		}
	}

	private function deactivate_all_cards()
	{
		$data = array('active' => "No");

		$query = $this->db->get('card');

		foreach ($query->result() as $row) 
		{
			$id = $row->id;
			$this->db->where('id', $id);
			$this->db->update('card', $data); 
		}
	}

	/* End private functions */

	public function index()
	{	
		$title ="Home";
		$this->load->vars(array('title' => $title) );

		if (new DateTime() > new DateTime("2016-08-21 23:59:59")) {
			$this->deactivate_all_cards();
		} else {
			$this->deactivate_old_cards();
		}

		$this->cancel_old_authorisations();

		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}

	public function sports()
	{
		$title ="Sports";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('sport');
		$crud->set_theme('datatables');

		$crud->set_subject('Sport');
		$crud->required_fields('name', 'events_start', 'events_finish');

		$output = $crud->render();
		$this->sports_output($output);
	}

	function sports_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('sports_view.php', $output);
		$this->load->view('footer');
	}

	public function venues()
	{
		$title ="Venues";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('venue');
		$crud->set_theme('datatables');

		$crud->set_subject('Venue');
		$crud->required_fields('name');

		$crud->add_action('Auths', '', '','ui-icon-folder-collapsed',array($this,'venue_authorisations'));

		$output = $crud->render();
		$this->venues_output($output);
	}

	function venues_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('venues_view.php', $output);
		$this->load->view('footer');
	}

	function venue_authorisations($primary_key) 
	{
		return site_url('main/list_venue_authorisations/'.$primary_key);
	}

	public function list_venue_authorisations($primary_key) 
	{	
		$this->db->select('name');
		$this->db->from('venue');
		$this->db->where('id =', $primary_key);
		$query = $this->db->get();

		if ($query->num_rows() > 0) 
		{
			$row = $query->row(); 

			$venue_name = $row->name;
		}

		$title ="Venue Authorisations List - " . $venue_name;
		$this->load->vars(array('title' => $title) );

		$this->load->view('header');

		$this->db->select('official.title, official.name, card.id, fixture.event, fixture.date');
		$this->db->from('authorisation');
		$this->db->join('fixture', 'authorisation.fixture_id = fixture.id', 'inner');
		$this->db->join('venue', 'fixture.venue_id = venue.id', 'inner');
		$this->db->join('card', 'authorisation.card_id = card.id', 'inner');
		$this->db->join('official', 'official.id = card.official_id', 'inner');
		$this->db->where('venue.id =', $primary_key);
		$query = $this->db->get();
		$data['authorisations'] = $query->result_array();

		$this->load->view('list_venue_authorisations', $data);
		$this->load->view('footer');
	}

	public function fixtures()
	{
		$title ="Fixtures";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('fixture');
		$crud->set_theme('datatables');

		$crud->set_subject('Fixture');
		$crud->required_fields('venue_id', 'sport_id', 
			'gender', 'event', 'date');

		$crud->set_relation('sport_id','sport','name');
		$crud->set_relation('venue_id','venue','name');

		$crud->display_as('sport_id', 'Sport');
		$crud->display_as('venue_id', 'Venue');

		$crud->field_type('gender','dropdown',
            array('M' => 'M', 'W' => 'W'));

		$output = $crud->render();
		$this->fixtures_output($output);
	}

	function fixtures_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('fixtures_view.php', $output);
		$this->load->view('footer');
	}

	public function officials()
	{
		$title ="Officials";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('official');
		$crud->set_theme('datatables');

		$crud->set_subject('Official');
		$crud->required_fields('sport_id', 'title', 'name');

		$crud->set_relation('sport_id','sport','name');

		$crud->display_as('sport_id', 'Sport');

		$crud->field_type('title','dropdown',
            array('Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs', 'Dr' => 'Dr'));

		$crud->set_lang_string('insert_success_message', 'Your data has been successfully stored into the database. An ID card has been automatically created for this official.');

		$crud->callback_after_insert(array($this, 'create_card_after_insert'));

		$output = $crud->render();
		$this->sports_output($output);
	}

	function officials_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('officials_view.php', $output);
		$this->load->view('footer');
	}

	/* Callbacks for officials */

	function create_card_after_insert($post_array, $primary_key)
	{
		$sport_id = $post_array['sport_id'];
		$valid_from = $this->find_valid_from_date($sport_id);
		$valid_until = $this->find_valid_until_date($sport_id);

		$card_insert = array(
			"official_id" => $primary_key,
			"active" => "Yes",
			"valid_from" => $valid_from,
			"valid_until" => $valid_until
		);

		$this->db->insert('card', $card_insert);

		return true;
	}

	/* End callbacks */

	public function cards()
	{
		$title ="Cards";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('card');
		$crud->set_theme('datatables');

		$crud->set_subject('Card');
		$crud->required_fields('active', 'official_id');

		$crud->set_relation('official_id','official','name');

		$crud->display_as('official_id', 'Official');
		$crud->columns('id','active', 'official_id', 'valid_from', 'valid_until'); 

		$crud->field_type('active','dropdown',
            array('Yes' => 'Yes', 'No' => 'No'));

		$crud->callback_after_insert(array($this, 'generate_dates_after_insert'));

		$output = $crud->render();
		$this->cards_output($output);
	}

	function cards_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('cards_view.php', $output);
		$this->load->view('footer');
	}

	/* Callbacks for cards */

	function generate_dates_after_insert($post_array, $primary_key)
	{
		$official_id = $post_array['official_id'];

		$this->db->select('sport_id');
		$this->db->from('official');
		$this->db->where('official.id =', $official_id);
		$query = $this->db->get();

		if ($query->num_rows() > 0) 
		{
			$row = $query->row(); 
			$sport_id = $row->sport_id;
		}

		$valid_from = $this->find_valid_from_date($sport_id);
		$valid_until = $this->find_valid_until_date($sport_id);

		$card_update = array(
			"valid_from" => $valid_from,
			"valid_until" => $valid_until
		);

		$this->db->where('id', $primary_key);
		$this->db->update('card', $card_update); 

		return true;
	}

	/* End callbacks */

	public function authorisations()
	{
		$title ="Authorisations";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('authorisation');
		$crud->set_theme('datatables');

		$crud->set_subject('Authorisation');
		$crud->required_fields('card_id', 'fixture_id');

		$crud->set_relation('card_id','card','id');
		$crud->set_relation('fixture_id','fixture', '{gender} {event} - {date}');

		$crud->display_as('card_id', 'Card');
		$crud->display_as('fixture_id', 'Fixture');

		$output = $crud->render();
		$this->authorisations_output($output);
	}

	function authorisations_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('authorisations_view.php', $output);
		$this->load->view('footer');
	}

	public function accesses()
	{
		$title ="Accesses";
		$this->load->vars(array('title' => $title) );

		$crud = new grocery_CRUD();
		$crud->set_table('access');
		$crud->set_theme('datatables');

		$crud->set_subject('Access');
		$crud->required_fields('card_id', 'fixture_id', 'authorised', 'time');

		$crud->set_relation('card_id','card','id');
		$crud->set_relation('fixture_id','fixture', '{gender} {event} - {date}');

		$crud->display_as('card_id', 'Card');
		$crud->display_as('fixture_id', 'Fixture');

		$crud->field_type('authorised','dropdown',
            array('Yes' => 'Yes', 'No' => 'No'));

		$output = $crud->render();
		$this->accesses_output($output);
	}

	function accesses_output($output = null)
	{
		$this->load->view('header', $output);
		$this->load->view('accesses_view.php', $output);
		$this->load->view('footer');
	}
}
